import Dependencies.Libraries._

resolvers += Resolver.sonatypeRepo("releases")
resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
    "com.pauldijou" %% "jwt-circe" % "4.2.0"
)

libraryDependencies += "com.github.t3hnar" %% "scala-bcrypt" % "4.1"

lazy val root = (project in file("."))
  .settings(
    organization := "juliano",
    name := "MyProject",
    version := "0.0.1",
    scalaVersion := "2.13.2",
    maxErrors := 3,
    libraryDependencies ++= Seq(
        zio,
        zioStreams,
        zioMacros,
        zioInteropCats,
        zioLogging,
        http4sServer,
        http4sDsl,
        http4sClient,
        http4sCirce,
        circeCore,
        circeGeneric,
        circeParser,
        quillJdbc,
        doobieCore,
        doobieQuill,
        doobieH2,
        pureConfig,
        h2,
        logback,

        zioTestSbt
    ),
    testFrameworks := Seq(new TestFramework("zio.test.sbt.ZTestFramework"))
  )

scalacOptions ++= Seq(
    //"-Xfatal-warnings",
    "-Ymacro-annotations"
)