package UserTest
import streaming.domain._
import streaming.environment.Environments.dbTransactor
import streaming.environment.config.Configuration
import streaming.environment.repository.UserStorage.UserRepository
import streaming.environment.repository.{DatabaseRepository, DbTransactor, User}
import zio.{Cause, ULayer}
import zio.blocking.Blocking
import zio.test.Assertion._
import zio.test._
import zio.test.environment.TestEnvironment

import java.util.UUID

object UserTest extends DefaultRunnableSpec {

  def spec =
    suite("Persistence unit test")(
      testM("create a user then get it ") {
        val test =
        for {
          _ <- User.createDB
          uuid = UUID.randomUUID()
          _ <- User.registerUser(InnerRegister(uuid, "test", "test"))
          token  <- User.loginUser(Login("test", "test"))
          userId  <- User.getUUID(token)
        } yield assert(userId)(equalTo(uuid))
        val dbTransactor: ULayer[DbTransactor] = Configuration.live >>> DbTransactor.h2
        test.provideLayer(dbTransactor >>> UserRepository.live)
      }
    )

}