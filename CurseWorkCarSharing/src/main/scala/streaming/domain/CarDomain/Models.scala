package streaming.domain.CarDomain

import java.util.UUID

final case class CarLogin (login: String, password: String)

final case class CarInfo (token: String, latitude: Double, longitude: Double, fuel: Double)

final case class CarRegister (login: String, password: String, brand: String, model: String)

