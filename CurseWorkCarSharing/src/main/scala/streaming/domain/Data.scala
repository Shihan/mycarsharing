package streaming.domain

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.UUID

final case class Register (login: String, password: String)

final case class InnerRegister(uuid: UUID, login: String, password: String)

final case class Login (login: String, password: String)

final case class wrongDataException() extends Exception

final case class loginWrongDataException() extends Exception

final case class AuthData (id: UUID)

final case class AuthCheck (id: UUID, password: String)

final case class Distance (latitude: Double, longitude: Double, distance: Double)

final case class CarFullInfo (id: UUID, brand: String, model: String,
                              latitude: Double, longitude: Double, fuel: Double, status: String)

final case class Token (token: String)

final case class TokenExpiredException() extends Exception

final case class CarLogin (login: String, password: String)

final case class CarInfo (token: String, latitude: Double, longitude: Double, fuel: Double)

final case class CarRegister (login: String, password: String, brand: String, model: String)

final case class CarFeedback (status: String)

final case class RentCar (token: String, carId: UUID)

final case class RentInfo(carId: UUID, start_time: String)

final case class RentInProcess() extends Exception

final case class UserNotFound(id: UUID) extends Exception

final case class RentNotFound(user_id: UUID) extends Exception
