package streaming.domain.UserDomain

import java.util.UUID

final case class Register (login: String, password: String)

final case class Login (login: String, password: String)

final case class AuthData (id: UUID)

final case class AuthCheck (id: UUID, password: String)

final case class Distance (latitude: Double, longitude: Double, distance: Double)

final case class CarFullInfo (id: UUID, brand: String, model: String,
                              latitude: Double, longitude: Double, fuel: Double, status: String)
