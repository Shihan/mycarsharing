package streaming.domain.UserDomain

import java.util.UUID

final case class wrongDataException() extends Exception

final case class loginWrongDataException() extends Exception

final case class TokenExpiredException() extends Exception

final case class RentInProcess() extends Exception

final case class UserNotFound(id: UUID) extends Exception
