package streaming.environment.repository

import doobie.implicits.toSqlInterpolator

object DBInit {
  val query = sql"""
                   |DROP TABLE IF EXISTS car_table cascade;
                   |DROP TABLE IF EXISTS user_table cascade;
                   |DROP TABLE IF EXISTS rent_table cascade;
                   |
                   |
                   |
                   |CREATE TABLE IF NOT EXISTS user_table (
                   |    id UUID default random_uuid() primary key,
                   |    login varchar UNIQUE NOT NULL,
                   |    password varchar NOT NULL
                   |);
                   |
                   |
                   |
                   |CREATE TABLE IF NOT EXISTS car_table (
                   |    id UUID default random_uuid() primary key,
                   |    login varchar UNIQUE NOT NULL,
                   |    password varchar NOT NULL,
                   |    brand varchar,
                   |    model varchar,
                   |    latitude DOUBLE,
                   |    longitude DOUBLE,
                   |    fuel DOUBLE,
                   |    status varchar
                   |);
                   |
                   |INSERT INTO car_table (login, password, brand, model, latitude, longitude, fuel, status)
                   |VALUES ('test', 'test', 'tesla', 'modelB', 1.0, 1.0, 10.0, 'free');
                   |
                   |CREATE TABLE IF NOT EXISTS rent_table (
                   |    id UUID default random_uuid() primary key,
                   |    user_id UUID NOT NULL,
                   |    car_id UUID NOT NULL,
                   |    start_time TIMESTAMP NOT NULL,
                   |    end_time TIMESTAMP
                   |);
                   |
                   |
                   |
                   |
                   |COMMIT;
                   |
                   |ANALYZE;
                   |""".stripMargin.update.run
}
