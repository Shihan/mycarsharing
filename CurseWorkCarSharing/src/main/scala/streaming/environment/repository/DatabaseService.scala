package streaming.environment.repository

import doobie.implicits._
import doobie.quill.DoobieContext
import doobie.util.transactor.Transactor
import io.getquill._
import zio.Task
import zio.interop.catz._

private[repository] final case class DatabaseService(xa: Transactor[Task]) extends DatabaseRepository.Service {
  val ctx = new DoobieContext.H2(Literal)


  override def initDB(): Task[Int] =
    DBInit.query.transact(xa)


}