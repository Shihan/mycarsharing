package streaming.environment.repository.UserStorage

import streaming.domain._
import streaming.environment.repository.{DbTransactor, UserRepository}
import zio._

import java.util.UUID

object UserRepository {
  trait Service {

    def getByToken(token: Token): Task[UUID]

    def registerUser(register: InnerRegister): Task[Unit]

    def loginUser(login: Login): Task[Token]

    def getCars(distance: Distance): Task[Seq[CarFullInfo]]

    def rentCar(rentCar: RentCar): Task[RentInfo]

    def getCurrentRent(token: Token): Task[RentInfo]

    def endRent(token: Token): Task[Unit]

    def initDB(): Task[Int]
  }

  val live: URLayer[DbTransactor, UserRepository] =
    ZLayer.fromService { resource =>
      UserService(resource.xa)
    }
}
