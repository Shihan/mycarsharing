package streaming.environment.repository.UserStorage

import doobie.util.transactor.Transactor
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}
import streaming.domain._
import zio.Task
import doobie.implicits._
import doobie.h2.implicits._
import io.circe.syntax.EncoderOps
import zio.interop.catz._
import io.circe.generic.auto._
import io.circe.parser.decode
import com.github.t3hnar.bcrypt._
import streaming.environment.repository.DBInit

import java.sql.Timestamp
import java.time.{Instant, LocalDateTime}
import java.util.UUID
import scala.util.Success

private[repository] final case class UserService(xa: Transactor[Task]) extends UserRepository.Service {

  private object JWToken {

    val secretKey = "secret_key"
    val algo: JwtAlgorithm.HS256.type = JwtAlgorithm.HS256

    def genToken(uuid: UUID): Token = {
      val claim = JwtClaim(
        content = AuthData(uuid).asJson.toString(),
        expiration = Some(
          Instant.now.plusSeconds(2419200).getEpochSecond
        ),
        issuedAt = Some(Instant.now.getEpochSecond)
      )
      val tokenStr = JwtCirce.encode(
        claim,
        secretKey,
        algo
      )
      Token(tokenStr)
    }

    def validateToken(token: String): Task[Option[UUID]] = Task {
      JwtCirce.decode(token, secretKey, Seq(algo)) match {
        case Success(claim: JwtClaim) =>
          claim.expiration match {
            case Some(time) if time > Instant.now.getEpochSecond =>
              decode[AuthData](claim.content) match {
                case Right(data) => Some(data.id)
                case Left(_) => None
              }
            case _ => throw TokenExpiredException()
          }
        case _ => None
      }
    }
  }


  private object Queries {

    def byId(id: UUID): Task[Option[UUID]] =
      sql"SELECT id FROM user_table WHERE id = $id"
        .query[UUID]
        .option
        .transact(xa)

    def createNew(register: InnerRegister): Task[Int] =
      sql"INSERT INTO user_table (id, login, password) VALUES (${register.uuid}, ${register.login}, ${register.password.bcrypt})"
        .update
        .run
        .transact(xa)

    def loginUser(login: Login): Task[Option[AuthCheck]] =
      sql"SELECT id, password FROM user_table WHERE login = ${login.login}"
        .query[AuthCheck]
        .option
        .transact(xa)

    def getCars(distance: Distance): Task[Seq[CarFullInfo]] =
      sql"""SELECT id, brand, model, latitude, longitude, fuel, status,
           |         ((ACOS(SIN(${distance.latitude} * PI() / 180) *
           |         SIN(latitude * PI() / 180) +
           |         COS(${distance.latitude} * PI() / 180) *
           |         COS(latitude * PI() / 180) *
           |         COS((${distance.longitude} - longitude) *
           |         PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609) AS distance
           |FROM car_table
           |WHERE status = 'free'
           |GROUP BY id
           |HAVING distance<=${distance.distance}""".stripMargin
        .query[CarFullInfo]
        .stream.compile.to(Seq)
        .transact(xa)

    def checkTakeCar(userId: UUID, carId: UUID): Task[Option[UUID]] =
      sql"SELECT id FROM rent_table WHERE end_time IS NULL AND (user_id = $userId OR car_id = $carId)"
        .query[UUID]
        .option
        .transact(xa)


    def takeCar(userId: UUID, carId: UUID): Task[Int] =
      sql"INSERT INTO rent_table (user_id, car_id, start_time) VALUES ($userId, $carId, ${Timestamp.valueOf(LocalDateTime.now()).toString})"
        .update
        .run
        .transact(xa)

    def getCurrentRent(userId: UUID): Task[Option[RentInfo]] =
      sql"SELECT car_id, start_time FROM rent_table WHERE end_time IS NULL AND user_id = $userId"
        .query[RentInfo]
        .option
        .transact(xa)

    def endRent(userId: UUID): Task[Int] =
      sql"""UPDATE rent_table SET end_time = ${Timestamp.valueOf(LocalDateTime.now()).toString}
            WHERE user_id = $userId AND end_time IS NULL"""
        .update
        .run
        .transact(xa)

  }

  override def getByToken(token: Token): Task[UUID] = {
    JWToken.validateToken(token.token).flatMap {
      case Some(uuid: UUID) =>
        Task.require(UserNotFound(uuid))(Queries.byId(uuid))
      case None => Task(throw wrongDataException())
    }
  }


  override def registerUser(register: InnerRegister): Task[Unit] =
    Queries.createNew(register).unit

  override def loginUser(login: Login): Task[Token] =
    Task.require(loginWrongDataException())(
      Queries.loginUser(login)
        .map {
          case Some(value) if login.password.isBcrypted(value.password) =>
            Some(JWToken.genToken(value.id))
          case _ => Option.empty[Token]
        }
    )

  override def getCars(distance: Distance): Task[Seq[CarFullInfo]] =
    Queries.getCars(distance)

  override def rentCar(rentCar: RentCar): Task[RentInfo] =
    JWToken.validateToken(rentCar.token).flatMap {
      case Some(uuid: UUID) =>
        Task.require(RentNotFound(uuid))(
          Queries.checkTakeCar(uuid, rentCar.carId).flatMap {
            case Some(_) => Task.fail(RentInProcess())
            case None => Queries.takeCar(uuid, rentCar.carId)
          }
            .flatMap(_ => Queries.getCurrentRent(uuid))
        )
      case None => Task(throw wrongDataException())
    }

  override def getCurrentRent(token: Token): Task[RentInfo] =
    JWToken.validateToken(token.token).flatMap {
      case Some(uuid: UUID) =>
        Task.require(RentNotFound(uuid))(
          Queries.getCurrentRent(uuid)
        )
      case None => Task(throw wrongDataException())

    }

  override def endRent(token: Token): Task[Unit] =
    JWToken.validateToken(token.token).flatMap {
      case Some(uuid: UUID) =>
        Queries.endRent(uuid).unit
      case None => Task(throw wrongDataException())
    }

  override def initDB(): Task[Int] =
    DBInit.query.transact(xa)

}
