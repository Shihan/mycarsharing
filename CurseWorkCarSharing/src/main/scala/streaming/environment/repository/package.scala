package streaming.environment

import streaming.domain._
import streaming.environment.config.Configuration.DbConfig
import doobie.util.transactor.Transactor
import streaming.environment.repository.CarStorage.CarRepository
import streaming.environment.repository.UserStorage.UserRepository
import zio._
import zio.interop.catz._

import java.util.UUID

package object repository {

  type DbTransactor = Has[DbTransactor.Resource]
  type DatabaseRepository = Has[DatabaseRepository.Service]
  type UserRepository = Has[UserRepository.Service]
  type CarRepository = Has[CarRepository.Service]

  object User{
    def registerUser(register: InnerRegister): RIO[UserRepository, Unit] =
      RIO.accessM(_.get.registerUser(register))

    def loginUser(login: Login): RIO[UserRepository, Token] =
      RIO.accessM(_.get.loginUser(login))

    def getUUID(token: Token): RIO[UserRepository, UUID] =
      RIO.accessM(_.get.getByToken(token))

    def getCars(distance: Distance): RIO[UserRepository, Seq[CarFullInfo]] =
      RIO.accessM(_.get.getCars(distance))

    def rentCar(rentCar: RentCar): RIO[UserRepository, RentInfo] =
      RIO.accessM(_.get.rentCar(rentCar))

    def getCurrentRent(token: Token): RIO[UserRepository, RentInfo] =
      RIO.accessM(_.get.getCurrentRent(token))

    def endRent(token: Token): RIO[UserRepository, Unit] =
      RIO.accessM(_.get.endRent(token))

    def createDB: RIO[UserRepository, Int] =
      RIO.accessM(_.get.initDB())
  }

  object Car{
    def registerCar(register: CarRegister): RIO[CarRepository, Unit] =
      RIO.accessM(_.get.registerCar(register))

    def loginCar(login: CarLogin): RIO[CarRepository, Token] =
      RIO.accessM(_.get.loginCar(login))

    def getUUID(token: Token): RIO[CarRepository, UUID] =
      RIO.accessM(_.get.getByToken(token))

    def update(carInfo: CarInfo): RIO[CarRepository, CarFeedback] =
      RIO.accessM(_.get.updateCar(carInfo))
  }

  def createDB: RIO[DatabaseRepository, Int] =
    RIO.accessM(_.get.initDB())


  object DbTransactor {
    trait Resource {
      val xa: Transactor[Task]
    }

    val h2: URLayer[Has[DbConfig], DbTransactor] = ZLayer.fromService { db =>
      new Resource {
        val xa: Transactor[Task] =
          Transactor.fromDriverManager(db.driver, db.url, db.user, db.password)
      }
    }
  }
}
