package streaming.environment.repository.CarStorage

import streaming.domain._
import streaming.environment.repository.{CarRepository, DbTransactor}
import zio.{Task, URLayer, ZLayer}

import java.util.UUID

object CarRepository {
  trait Service {
    def getByToken(token: Token): Task[UUID]

    def registerCar(register: CarRegister): Task[Unit]

    def loginCar(login: CarLogin): Task[Token]

    def updateCar(info: CarInfo): Task[CarFeedback]
  }

  val live: URLayer[DbTransactor, CarRepository] =
    ZLayer.fromService { resource =>
      CarService(resource.xa)
    }
}
