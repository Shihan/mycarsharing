package streaming.environment.repository.CarStorage

import doobie.util.transactor.Transactor
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}
import streaming.domain._
import zio.Task
import doobie.implicits._
import doobie.h2.implicits._
import io.circe.syntax.EncoderOps
import zio.interop.catz._
import io.circe.generic.auto._
import io.circe.parser.decode
import com.github.t3hnar.bcrypt._

import java.time.Instant
import java.util.UUID
import scala.util.Success

private[repository] final case class CarService(xa: Transactor[Task]) extends CarRepository.Service {

  private object JWToken {

    val secretKey = "secret_key"
    val algo: JwtAlgorithm.HS256.type = JwtAlgorithm.HS256

    def genToken(uuid: UUID): Token = {
      val claim = JwtClaim(
        content = AuthData(uuid).asJson.toString(),
        expiration = Some(
          Instant.now.plusSeconds(2419200).getEpochSecond
        ),
        issuedAt = Some(Instant.now.getEpochSecond)
      )
      val tokenStr = JwtCirce.encode(
        claim,
        secretKey,
        algo
      )
      Token(tokenStr)
    }

    def validateToken(token: String): Task[Option[UUID]] = Task {
      JwtCirce.decode(token, secretKey, Seq(algo)) match {
        case Success(claim: JwtClaim) =>
          claim.expiration match {
            case Some(time) if time > Instant.now.getEpochSecond =>
              decode[AuthData](claim.content) match {
                case Right(data) => Some(data.id)
                case Left(_) => None
              }
            case _ => throw TokenExpiredException()
          }
        case _ => None
      }
    }
  }


  private object Queries {

    def byId(id: UUID): Task[Option[UUID]] =
      sql"SELECT id FROM car_table WHERE id = $id"
        .query[UUID]
        .option
        .transact(xa)

    def createNew(register: CarRegister): Task[Int] =

      sql"""INSERT INTO car_table (login, password, brand, model, latitude, longitude, fuel, status)
           VALUES (${register.login}, ${register.password.bcrypt}, ${register.brand}, ${register.model}, 0, 0, 0, 'free')"""
        .update
        .run
        .transact(xa)

    def loginCar(login: CarLogin): Task[Option[AuthCheck]] =
      sql"SELECT id, password FROM car_table WHERE login = ${login.login}"
        .query[AuthCheck]
        .option
        .transact(xa)

    def checkRent(id: UUID): Task[Option[UUID]] =
      sql"SELECT id FROM rent_table WHERE end_time IS NULL AND car_id = $id"
        .query[UUID]
        .option
        .transact(xa)

    def updateCar(id: UUID, info: CarInfo, status: String): Task[Int] =
      sql"""UPDATE car_table
           SET latitude = ${info.latitude}, longitude = ${info.longitude}, fuel = ${info.fuel}, status = $status
           WHERE id = ${id}"""
        .update
        .run
        .transact(xa)
  }

  override def getByToken(token: Token): Task[UUID] = {
    JWToken.validateToken(token.token).flatMap {
      case Some(uuid) =>
        Task.require(UserNotFound(uuid))(Queries.byId(uuid))
      case None => Task.fail(wrongDataException())
    }
  }

  override def registerCar(register: CarRegister): Task[Unit] =
    Queries.createNew(register).unit

  override def loginCar(login: CarLogin): Task[Token] =
    Task.require(loginWrongDataException())(
      Queries.loginCar(login)
        .map {
          case Some(value) if login.password.isBcrypted(value.password) =>
            Some(JWToken.genToken(value.id))
          case _ => Option.empty[Token]
        }
    )

  override def updateCar(info: CarInfo): Task[CarFeedback] =
    JWToken.validateToken(info.token).flatMap {
      case Some(uuid: UUID) =>
        Queries.checkRent(uuid)
          .map {
            case Some(_) => "in rent"
            case None => "free"
          }.flatMap { res =>
          Queries.updateCar(uuid, info, res)
            .foldM(
              err => Task.fail(err),
              _ => Task.succeed(CarFeedback(res))
            )
        }
      case None => Task(throw wrongDataException())
    }
}
