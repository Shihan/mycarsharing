package streaming.environment.repository


import zio._

object DatabaseRepository {

  trait Service {
    def initDB(): Task[Int]
  }

  val live: URLayer[DbTransactor, DatabaseRepository] =
    ZLayer.fromService { resource =>
      DatabaseService(resource.xa)
    }
}
