package streaming.environment

import streaming.environment.config.Configuration
import streaming.environment.repository.CarStorage.CarRepository
import streaming.environment.repository.UserStorage.UserRepository
import streaming.environment.repository.{DatabaseRepository, DbTransactor, UserRepository, CarRepository}
import zio.ULayer
import zio.clock.Clock

object Environments {
  type HttpServerEnvironment = Configuration with Clock
  type AppEnvironment = HttpServerEnvironment with DatabaseRepository with UserRepository with CarRepository

  val httpServerEnvironment: ULayer[HttpServerEnvironment] = Configuration.live ++ Clock.live
  val dbTransactor: ULayer[DbTransactor] = Configuration.live >>> DbTransactor.h2
  val databaseRepository: ULayer[DatabaseRepository] = dbTransactor >>> DatabaseRepository.live
  val userRepository: ULayer[UserRepository] = dbTransactor >>> UserRepository.live
  val carRepository: ULayer[CarRepository] = dbTransactor >>> CarRepository.live
  val appEnvironment: ULayer[AppEnvironment] = httpServerEnvironment >+> (databaseRepository >+> userRepository ++ carRepository)
}
