package streaming.http.endpoints

import io.circe.generic.auto._
import io.circe.{Decoder, Encoder}
import streaming.domain._
import streaming.environment.repository._
import io.circe.syntax._
import org.http4s.{EntityDecoder, EntityEncoder, HttpRoutes}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import zio.interop.catz._
import zio.RIO

import java.util.UUID

final class UserEndpoint[R <: UserRepository] {

  type UserTask[A] = RIO[R, A]

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[UserTask, A] =
    jsonOf[UserTask, A]
  implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[UserTask, A] =
    jsonEncoderOf[UserTask, A]

  private val prefixPath = "/user"

  val dsl = Http4sDsl[UserTask]
  import dsl._

  private val httpRoutes: HttpRoutes[UserTask] = {

    HttpRoutes.of[UserTask] {

      case request @ GET -> Root / "exist" =>
        request.decode[Token] { token =>
          User.getUUID(token)
            .foldM(_ => NotFound(), _ => Ok("ok"))
        }

      case request @ POST -> Root / "register" =>
        request.decode[Register] { register =>
          User.registerUser(InnerRegister(UUID.randomUUID(), register.login, register.password))
            .foldM(_ => BadRequest(), Created(_))
        }

      case request @ GET -> Root / "login" =>
        request.decode[Login] { login =>
          User.loginUser(login)
            .foldM(_ => NotFound(), Ok(_))
        }

      case request @ GET -> Root / "cars" =>
        request.decode[Distance] { distance =>
          User.getCars(distance)
            .foldM(_ => BadRequest(), Ok(_))
        }

      case request @ POST -> Root / "take" =>
        request.decode[RentCar] {rentCar =>
          User.rentCar(rentCar)
            .foldM(err => BadRequest(err.toString), Ok(_))
        }
      case request @ GET -> Root / "rent" =>
        request.decode[Token] { token =>
          User.getCurrentRent(token)
            .foldM(_ => BadRequest(), Ok(_))
        }
      case request @ POST -> Root / "end" =>
        request.decode[Token] { token =>
          User.endRent(token)
            .foldM(err => BadRequest(err.toString), _ => Ok("ok"))
        }
    }
  }


  val routes: HttpRoutes[UserTask] = Router(
    prefixPath -> httpRoutes
  )
}
