package streaming.http.endpoints

import io.circe.generic.auto._
import io.circe.{Decoder, Encoder}
import streaming.domain._
import streaming.environment.repository._
import io.circe.syntax._
import org.http4s.{EntityDecoder, EntityEncoder, HttpRoutes}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import zio.interop.catz._
import zio.RIO

final class CarEndpoint[R <: CarRepository] {

  type CarTask[A] = RIO[R, A]

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[CarTask, A] =
    jsonOf[CarTask, A]
  implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[CarTask, A] =
    jsonEncoderOf[CarTask, A]


  private val prefixPath = "/car"

  val dsl = Http4sDsl[CarTask]
  import dsl._

  private val httpRoutes: HttpRoutes[CarTask] = {

    HttpRoutes.of[CarTask] {

      case request @ GET -> Root / "exist" =>
        request.decode[Token] { token =>
          Car.getUUID(token)
            .foldM(_ => NotFound(), _ => Ok("ok"))
        }

      case request @ POST -> Root / "register" =>
        request.decode[CarRegister] { register =>
          Car.registerCar(register)
            .foldM(_ => BadRequest(), Created(_))
        }

      case request @ GET -> Root / "login" =>
        request.decode[CarLogin] { login =>
          Car.loginCar(login)
            .foldM(_ => NotFound(), Ok(_))

        }

      case request @ POST -> Root / "update" =>
        request.decode[CarInfo] { carInfo =>
          Car.update(carInfo)
            .foldM(_ => BadRequest(), Ok(_))
        }
    }
  }

  val routes: HttpRoutes[CarTask] = Router(
    prefixPath -> httpRoutes
  )
}
