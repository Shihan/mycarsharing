
ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "testCurse"
  )

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime

libraryDependencies ++= Seq(
  "io.getquill"          %% "quill-jdbc-zio" % "3.12.0",
  "io.github.kitlangton" %% "zio-magic"      % "0.3.11",
  "org.postgresql"       %  "postgresql"     % "42.3.1"
)

libraryDependencies ++= Seq(
  //"io.getquill" %% "quill-jdbc" % "3.4.10",
  "org.postgresql" % "postgresql" % "42.2.8",
  "com.opentable.components" % "otj-pg-embedded" % "0.13.1"
)
///////////////////////////////////////
val Http4sVersion     = "0.21.22"
val CirceVersion      = "0.13.0"
val DoobieVersion     = "0.12.1"
val ZIOVersion        = "1.0.6"
val PureConfigVersion = "0.15.0"
val ZIOInterop        = "2.4.0.0"

libraryDependencies ++= Seq(
  // ZIO
  "com.softwaremill.sttp.client3" %% "core" % "3.3.13",
  "com.softwaremill.sttp.client3" %% "async-http-client-backend-zio1" % "3.6.2",
  "com.softwaremill.sttp.client3" %% "circe" % "3.3.13",
  "com.softwaremill.sttp" %% "async-http-client-backend-future" % "1.7.2",
  "io.circe" %% "circe-generic" % "0.14.1"
)

scalacOptions ++= Seq(
  "-deprecation",
  "-unchecked",
  "-feature",
  "-Xlint"
)
