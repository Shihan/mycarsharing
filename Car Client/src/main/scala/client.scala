import io.circe.{Decoder, Encoder}
import sttp.client3.quick._
import sttp.client3.circe._
import io.circe.generic.auto._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

class client {
  def getToken(): String = {

    final case class Response(token: String)
    final case class Login(login: String, password: String)

    val url = uri"http://127.0.0.1:8080/api/v1/car/login"

    val data = Login("test1", "test1")

    val sttpRequest = basicRequest
      .body(data)
      .get(url)
      .response(asJson[Response])

    val res = sttpRequest
      .send(backend)

    Thread.sleep(3000)

    res.body match {
      case Right(response) => response.token
      case Left(err) => err.toString
    }
  }

  def updateInfo(token: String): String = {
    final case class Response(status: String)
    final case class Update(token: String, latitude: Double, longitude: Double, fuel: Double)

    val url = uri"http://127.0.0.1:8080/api/v1/car/update"

    val data = Update(token, 0, 0, 1)

    val sttpRequest = basicRequest
      .body(data)
      .post(url)
      .response(asJson[Response])

    val res = sttpRequest
      .send(backend)

    Thread.sleep(3000)

    res.body match {
      case Right(response) => response.status
      case Left(err) => err.toString
    }
  }
}
